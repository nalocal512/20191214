Photos from the NAlocal512 MeetShoot #001

### Adding photos to this album

This is really easy if you are a member of the NAlocal512 gitlab group,
merely visit this page:

https://gitlab.com/nalocal512/20191214/tree/master/content/images

and upload images (click the '+' sign right after the 'content / images'
and choose 'Upload file').  After you are done the site will be automagically re-built with
your images included.
